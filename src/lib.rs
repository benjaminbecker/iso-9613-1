const p_Reference: f32 = 101.325; // kPa
const T_Reference: f32 = 293.15; // K

pub struct Atmosphere {
    relative_humidity: f32, // in percent
    atmospheric_pressure: f32, // in kPa
    temperature: f32, // in K
}

impl Atmosphere {
    fn molar_water_concentration(&self) -> f32 {
        let T_01 = 273.16;
        let C = -6.8346 * (T_01 / self.temperature).powf(1.261) + 4.6151;
        self.relative_humidity * (10.0 as f32).powf(C) * self.atmospheric_pressure / p_Reference
    }

    fn relaxation_frequency_oxygen(&self) -> f32 {
        let h = self.molar_water_concentration();
        self.atmospheric_pressure / p_Reference 
        * (24. + 4.04 * 1e4 * h
            * (0.02 + h) / (0.391 + h)
        )
    }

    fn relaxation_frequency_nitrogen(&self) -> f32 {
        self.atmospheric_pressure / p_Reference 
        * (self.temperature / T_Reference).powf(-1./2.)
        * (9. + 280. * self.molar_water_concentration()
            * (-4.17 * (self.temperature / T_Reference).powf(-1./3.) - 1.).exp()
        )
    }

    pub fn attenuation(&self, frequency: f32) -> f32 {
        let f_rO = self.relaxation_frequency_oxygen();
        let f_rN = self.relaxation_frequency_nitrogen();
        8.686 * frequency.powi(2)
        * (
            (1.84e-11 * (self.atmospheric_pressure / p_Reference).powi(-1) * (self.temperature / T_Reference).powf(1./2.))
            + (self.temperature / T_Reference).powf(-5./2.)
            * (
                0.01275 * (-2239.1 / self.temperature).exp() * (f_rO + frequency.powi(2) / f_rO).powi(-1)
                + 0.1068 * (-3352.0 / self.temperature).exp() * (f_rN + frequency.powi(2) / f_rN).powi(-1)
            )
        )
    }
}






#[cfg(test)]
mod tests {
    use super::*;
    use assert_approx_eq::assert_approx_eq;
    #[test]
    fn it_works_for_0C_20percent() {
        let atmosphere = Atmosphere {
            atmospheric_pressure: 101.325,
            temperature: 273.15,
            relative_humidity: 20.
        };

        assert_approx_eq!(
            atmosphere.attenuation(800.) * 1000., 
            12.9, 
            1e-1
        );
        assert_approx_eq!(
            atmosphere.attenuation(8000.) * 1000., 
            58.1, 
            1e-1
        );
    }

    #[test]
    fn it_works_for_minus20C_50percent() {
        let atmosphere = Atmosphere {
            atmospheric_pressure: 101.325,
            temperature: 273.15 - 20.,
            relative_humidity: 50.
        };

        assert_approx_eq!(
            atmosphere.attenuation(800.) * 1000., 
            8.22, 
            1e-2
        );
        assert_approx_eq!(
            atmosphere.attenuation(8000.) * 1000., 
            20.3, 
            1.
        );
    }
}
